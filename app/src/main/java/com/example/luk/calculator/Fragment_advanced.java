package com.example.luk.calculator;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by LUK on 2015-03-22.
 */
public class Fragment_advanced extends Fragment  implements View.OnClickListener {

    Button one, two, three, four, five, six, seven, eight, nine, zero, add, sub, mul, div,c,bks,dot,pm,equ,sin,cos,tan,ln,sqr,x2,xy,log;
    float op1;
    float op2;
    String optr;
    EditText disp;
    String zmienna="",zmienna2="",znak="";



    View view;
    TextView textv;





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_advanced, container, false);
        textv=((TextView) view.findViewById (R.id.textAdvance));



        one = (Button) view.findViewById(R.id.Advance1);
        two = (Button) view.findViewById(R.id.Advance2);
        three = (Button) view.findViewById(R.id.Advance3);
        four = (Button) view.findViewById(R.id.Advance4);
        five = (Button) view.findViewById(R.id.Advance5);
        six = (Button) view.findViewById(R.id.Advance6);
        seven = (Button) view.findViewById(R.id.Advance7);
        eight = (Button) view.findViewById(R.id.Advance8);
        nine = (Button) view.findViewById(R.id.Advance9);
        zero = (Button) view.findViewById(R.id.Advance0);
        add = (Button) view.findViewById(R.id.AdvancePlus);
        sub = (Button) view.findViewById(R.id.AdvanceMinus);
        mul = (Button) view.findViewById(R.id.AdvanceMul);
        div = (Button) view.findViewById(R.id.AdvanceDiv);
        equ = (Button) view.findViewById(R.id.AdvanceEquel);
        c = (Button) view.findViewById(R.id.AdvanceC);
        bks = (Button) view.findViewById(R.id.AdvanceBksp);
        dot = (Button) view.findViewById(R.id.AdvanceDot);
        pm = (Button) view.findViewById(R.id.Advance18);
        sin = (Button) view.findViewById(R.id.AdvanceSin);
        cos = (Button) view.findViewById(R.id.AdvanceCos);
        tan = (Button) view.findViewById(R.id.AdvanceTan);
        ln = (Button) view.findViewById(R.id.AdvanceLn);
        sqr = (Button) view.findViewById(R.id.AdvanceSqrt);
        x2 = (Button) view.findViewById(R.id.AdvanceX2);
        xy = (Button) view.findViewById(R.id.AdvanceXy);
        log = (Button) view.findViewById(R.id.AdvanceLog);



        try{
            one.setOnClickListener(this);

            two.setOnClickListener(this);

            three.setOnClickListener(this);

            four.setOnClickListener(this);

            five.setOnClickListener(this);

            six.setOnClickListener(this);

            seven.setOnClickListener(this);

            eight.setOnClickListener(this);

            nine.setOnClickListener(this);

            zero.setOnClickListener(this);

            add.setOnClickListener(this);

            sub.setOnClickListener(this);

            mul.setOnClickListener(this);

            div.setOnClickListener(this);

            equ.setOnClickListener(this);

            c.setOnClickListener(this);
            bks.setOnClickListener(this);
            dot.setOnClickListener(this);
            pm.setOnClickListener(this);
            sin.setOnClickListener(this);
            cos.setOnClickListener(this);
            tan.setOnClickListener(this);
            ln.setOnClickListener(this);

            sqr.setOnClickListener(this);
            x2.setOnClickListener(this);
            xy.setOnClickListener(this);
            log.setOnClickListener(this);
        }
        catch(Exception e){

        }







        return view;
    }

    private void dodaj(){
        float skladnik1,skladnik2;
        if(zmienna2=="")
            skladnik1=0;
        else
            skladnik1 = Float.parseFloat(zmienna2);
        if (zmienna=="")
            skladnik2=0;
        else
            skladnik2 = Float.parseFloat(zmienna);

        zmienna=String.valueOf(skladnik1+skladnik2);
    }
    private void minus(){
        float odjemna ,odjemnik;
        if(zmienna2=="")
            odjemna =0;
        else
            odjemna  = Float.parseFloat(zmienna2);
        if (zmienna=="")
            odjemnik=0;
        else
            odjemnik = Float.parseFloat(zmienna);

        zmienna=String.valueOf(odjemna-odjemnik);
    }
    private void mnoz(){
        float czunnik1 ,czunnik2;
        if(zmienna2=="")
            czunnik1 =0;
        else
            czunnik1  =Float.parseFloat(zmienna2);
        if (zmienna=="")
            czunnik2=0;
        else
            czunnik2 = Float.parseFloat(zmienna);

        zmienna=String.valueOf(czunnik1*czunnik2);
    }
    private void dziel(){
        float dzielnik ,dzielna;
        if(zmienna2=="")
            dzielnik =0;
        else
            dzielnik  = Float.parseFloat(zmienna2);
        if (zmienna=="")
            dzielna=0;
        else
            dzielna = Float.parseFloat(zmienna);

        zmienna=String.valueOf(dzielnik/dzielna);
    }
    private void bksp(){
        if(zmienna.length()>0)
            zmienna=zmienna.substring(0,zmienna.length()-1);
    }
    private void zmiana(){
        if((zmienna!=""&&(zmienna!="+"))&&(zmienna!="-")&&(zmienna!="*")&&(zmienna!="/")) {
            float sq = Float.parseFloat(zmienna);
            sq *= -1;
            zmienna=String.valueOf(sq);
        }
    }


    private void doy(){
        float x ,y;
        if(zmienna2=="")
            x =0;
        else
            x  = Float.parseFloat(zmienna2);
        if (zmienna=="")
            y= 0;
        else
            y = Float.parseFloat(zmienna);

        zmienna=String.valueOf(Math.pow(x,y));

    }



    private void do2(){
        float x ;
        if(zmienna=="")
            x =0;
        else
            x  = Float.parseFloat(zmienna);

        zmienna=String.valueOf(Math.pow(x,2));

    }



    private void log(){
        float liczba;
        if(zmienna=="")
            liczba=0;
        else
            liczba = Float.parseFloat(zmienna);
        if(liczba>0)
            zmienna=String.valueOf(Math.log(liczba));
    }
    private void sin(){
        float liczba;
        if(zmienna=="")
            liczba=0;
        else
            liczba = Float.parseFloat(zmienna);
        zmienna=String.valueOf(Math.sin(liczba));
    }
    private void cos(){
        float liczba;
        if(zmienna=="")
            liczba=0;
        else
            liczba = Float.parseFloat(zmienna);
        zmienna=String.valueOf(Math.cos(liczba));
    }
    private void tan(){
        float liczba;
        if(zmienna=="")
            liczba=0;
        else
            liczba = Float.parseFloat(zmienna);
        zmienna=String.valueOf(Math.tan(liczba));
    }

    private void sqrt(){
        float liczba=0;
        if(zmienna=="")
            liczba =0;
        else
            liczba  = Float.parseFloat(zmienna);

        zmienna=String.valueOf(Math.sqrt(liczba));
    }


    @Override
    public void onClick (View v){
        switch(v.getId()) {
            case R.id.AdvanceDot:
                if(zmienna==""){
                    zmienna="0";
                }
                if(zmienna.contains(".")){}
                else
                    zmienna=zmienna+".";
                textv.setText(zmienna);
                break;
            case R.id.Advance18:
                zmiana();
                textv.setText(zmienna);
                break;

            case R.id.AdvanceC:
                zmienna="";
                zmienna2="";
                textv.setText(zmienna);
                break;

            case R.id.AdvanceBksp:
                bksp();
                textv.setText(zmienna);
                break;

            case R.id.AdvanceX2:
                do2();
                textv.setText(zmienna);
                break;

            case R.id.AdvanceXy:
                zmienna2= (String) textv.getText();
                zmienna="";
                znak = "^";

                textv.setText(zmienna);
                break;

            case R.id.AdvanceTan:
                tan();
                textv.setText(zmienna);
                break;
            case R.id.AdvanceCos:
                cos();
                textv.setText(zmienna);
                break;

            case R.id.AdvanceSin:
                sin();
                textv.setText(zmienna);
                break;
            case R.id.AdvanceLn:
                log();
                textv.setText(zmienna);
                break;

            case R.id.AdvanceSqrt:
                sqrt();
                textv.setText(zmienna);
                break;


            case R.id.AdvanceEquel:
                switch(znak) {

                    case "+":
                        dodaj();
                        znak="";
                        textv.setText(zmienna);
                        break;
                    case "-":
                        minus();
                        znak="";
                        textv.setText(zmienna);
                        break;
                    case "*":
                        mnoz();
                        znak="";
                        textv.setText(zmienna);
                        break;
                    case "/":
                        dziel();
                        znak="";
                        textv.setText(zmienna);
                        break;
                    case "^":
                        doy();
                        znak="";
                        textv.setText(zmienna);
                        break;
                    default:
                        break;
                }

                break;
            case R.id.AdvanceDiv:
                zmienna2= (String) textv.getText();
                zmienna="";
                znak = "/";
                textv.setText(div.getText());
                break;
            case R.id.AdvanceMul:
                zmienna2= (String) textv.getText();
                zmienna="";
                znak = "*";
                textv.setText(mul.getText());
                break;
            case R.id.AdvanceMinus:
                zmienna2= (String) textv.getText();
                zmienna="";
                znak = "-";
                textv.setText(sub.getText());
                break;
            case R.id.AdvancePlus:
                zmienna2= (String) textv.getText();
                zmienna="";
                znak = "+";
                textv.setText(add.getText());
                break;

            case R.id.Advance0:
                zmienna=zmienna+zero.getText();
                textv.setText(zmienna);
                break;
            case R.id.Advance1:
                zmienna=zmienna+one.getText();
                textv.setText(zmienna);
                break;
            case R.id.Advance2:
                zmienna=zmienna+two.getText();
                textv.setText(zmienna);
                break;
            case R.id.Advance3:
                zmienna=zmienna+three.getText();
                textv.setText(zmienna);
                break;
            case R.id.Advance4:
                zmienna=zmienna+four.getText();
                textv.setText(zmienna);
                break;
            case R.id.Advance5:
                zmienna=zmienna+five.getText();
                textv.setText(zmienna);
                break;
            case R.id.Advance6:
                zmienna=zmienna+six.getText();
                textv.setText(zmienna);
                break;
            case R.id.Advance7:
                zmienna=zmienna+seven.getText();
                textv.setText(zmienna);
                break;
            case R.id.Advance8:
                zmienna=zmienna+eight.getText();
                textv.setText(zmienna);
                break;
            case R.id.Advance9:
                zmienna=zmienna+nine.getText();
                textv.setText(zmienna);
                break;
            default:
                textv.setText("qqqqqqqqqqq");
                break;

        }
    }



}