package com.example.luk.calculator;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by LUK on 2015-03-21.
 */
public class Fragment_simple extends Fragment implements  View.OnClickListener{

    Button one, two, three, four, five, six, seven, eight, nine, zero, add, sub, mul, div,c,bks,dot,pm,equ;
    float op1;
    float op2;
    String optr;
    EditText disp;
    String zmienna="",zmienna2="",znak="";



    View view;
    TextView textv;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_simple, container, false);
        textv=((TextView) view.findViewById (R.id.textView1));



        one = (Button) view.findViewById(R.id.Simple1);
        two = (Button) view.findViewById(R.id.Simple2);
        three = (Button) view.findViewById(R.id.Simple3);
        four = (Button) view.findViewById(R.id.Simple4);
        five = (Button) view.findViewById(R.id.Simple5);
        six = (Button) view.findViewById(R.id.Simple6);
        seven = (Button) view.findViewById(R.id.Simple7);
        eight = (Button) view.findViewById(R.id.Simple8);
        nine = (Button) view.findViewById(R.id.Simple9);
        zero = (Button) view.findViewById(R.id.Simple0);
        add = (Button) view.findViewById(R.id.SimplePlus);
        sub = (Button) view.findViewById(R.id.SimpleMinus);
        mul = (Button) view.findViewById(R.id.SimpleMul);
        div = (Button) view.findViewById(R.id.SimpleDiv);
        equ = (Button) view.findViewById(R.id.SimpleEquel);
        c = (Button) view.findViewById(R.id.SimpleC);
        bks = (Button) view.findViewById(R.id.SimpleBksp);
        dot = (Button) view.findViewById(R.id.SimpleDot);
        pm = (Button) view.findViewById(R.id.Simple18);



        try{
            one.setOnClickListener(this);

            two.setOnClickListener(this);

            three.setOnClickListener(this);

            four.setOnClickListener(this);

            five.setOnClickListener(this);

            six.setOnClickListener(this);

            seven.setOnClickListener(this);

            eight.setOnClickListener(this);

            nine.setOnClickListener(this);

            zero.setOnClickListener(this);

            add.setOnClickListener(this);

            sub.setOnClickListener(this);

            mul.setOnClickListener(this);

            div.setOnClickListener(this);

            equ.setOnClickListener(this);

            c.setOnClickListener(this);
            bks.setOnClickListener(this);
            dot.setOnClickListener(this);
            pm.setOnClickListener(this);
        }
        catch(Exception e){

        }







        return view;
    }

    private void dodaj(){
        float skladnik1,skladnik2;
        if(zmienna2=="")
            skladnik1=0;
        else
            skladnik1 = Float.parseFloat(zmienna2);
        if (zmienna=="")
            skladnik2=0;
        else
            skladnik2 = Float.parseFloat(zmienna);

        zmienna=String.valueOf(skladnik1+skladnik2);
    }
    private void minus(){
        float odjemna ,odjemnik;
        if(zmienna2=="")
            odjemna =0;
        else
            odjemna  = Float.parseFloat(zmienna2);
        if (zmienna=="")
            odjemnik=0;
        else
            odjemnik = Float.parseFloat(zmienna);

        zmienna=String.valueOf(odjemna-odjemnik);
    }
    private void mnoz(){
        float czunnik1 ,czunnik2;
        if(zmienna2=="")
            czunnik1 =0;
        else
            czunnik1  =Float.parseFloat(zmienna2);
        if (zmienna=="")
            czunnik2=0;
        else
            czunnik2 = Float.parseFloat(zmienna);

        zmienna=String.valueOf(czunnik1*czunnik2);
    }
    private void dziel(){
        float dzielnik ,dzielna;
        if(zmienna2=="")
            dzielnik =0;
        else
            dzielnik  = Float.parseFloat(zmienna2);
        if (zmienna=="")
            dzielna=0;
        else
            dzielna = Float.parseFloat(zmienna);

        zmienna=String.valueOf(dzielnik/dzielna);
    }
    private void bksp(){
        if(zmienna.length()>0)
            zmienna=zmienna.substring(0,zmienna.length()-1);
    }
    private void zmiana(){
        if((zmienna!=""&&(zmienna!="+"))&&(zmienna!="-")&&(zmienna!="*")&&(zmienna!="/")) {
            float sq = Float.parseFloat(zmienna);
            sq *= -1;
            zmienna=String.valueOf(sq);
        }
    }
    @Override
    public void onClick (View v){
        switch(v.getId()) {
            case R.id.SimpleDot:
                if(zmienna==""){
                    zmienna="0";
                }
                if(zmienna.contains(".")){}
                else
                    zmienna=zmienna+".";
                textv.setText(zmienna);
                break;
            case R.id.Simple18:
                zmiana();
                textv.setText(zmienna);
                break;

            case R.id.SimpleC:
                zmienna="";
                zmienna2="";
                textv.setText(zmienna);
                break;

            case R.id.SimpleBksp:
                bksp();
                textv.setText(zmienna);
                break;

            case R.id.SimpleEquel:
                switch(znak) {
                    case "+":
                        dodaj();
                        znak="";
                        textv.setText(zmienna);
                        break;
                    case "-":
                        minus();
                        znak="";
                        textv.setText(zmienna);
                        break;
                    case "*":
                        mnoz();
                        znak="";
                        textv.setText(zmienna);
                        break;
                    case "/":
                        dziel();
                        znak="";
                        textv.setText(zmienna);
                        break;
                    default:
                        break;
                }

                break;
            case R.id.SimpleDiv:
                zmienna2= (String) textv.getText();
                zmienna="";
                znak = "/";
                textv.setText(div.getText());
                break;
            case R.id.SimpleMul:
                zmienna2= (String) textv.getText();
                zmienna="";
                znak = "*";
                textv.setText(mul.getText());
                break;
            case R.id.SimpleMinus:
                zmienna2= (String) textv.getText();
                zmienna="";
                znak = "-";
                textv.setText(sub.getText());
                break;
            case R.id.SimplePlus:
                zmienna2= (String) textv.getText();
                zmienna="";
                znak = "+";
                textv.setText(add.getText());
                break;

            case R.id.Simple0:
                zmienna=zmienna+zero.getText();
                textv.setText(zmienna);
                break;
            case R.id.Simple1:
                zmienna=zmienna+one.getText();
                textv.setText(zmienna);
                break;
            case R.id.Simple2:
                zmienna=zmienna+two.getText();
                textv.setText(zmienna);
                break;
            case R.id.Simple3:
                zmienna=zmienna+three.getText();
                textv.setText(zmienna);
                break;
            case R.id.Simple4:
                zmienna=zmienna+four.getText();
                textv.setText(zmienna);
                break;
            case R.id.Simple5:
                zmienna=zmienna+five.getText();
                textv.setText(zmienna);
                break;
            case R.id.Simple6:
                zmienna=zmienna+six.getText();
                textv.setText(zmienna);
                break;
            case R.id.Simple7:
                zmienna=zmienna+seven.getText();
                textv.setText(zmienna);
                break;
            case R.id.Simple8:
                zmienna=zmienna+eight.getText();
                textv.setText(zmienna);
                break;
            case R.id.Simple9:
                zmienna=zmienna+nine.getText();
                textv.setText(zmienna);
                break;
            default:
                textv.setText("qqqqqqqqqqq");
                break;

        }
    }



}
