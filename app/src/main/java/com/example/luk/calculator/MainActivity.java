package com.example.luk.calculator;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import android.view.View;


public class MainActivity extends Activity {
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentManager = getFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, new Fragment_menu());
        fragmentTransaction.commit();
    }


    @Override
    public void onBackPressed() {

  

        int count = getFragmentManager().getBackStackEntryCount();

        if (count == 1) {
            super.onBackPressed();
            //additional code
        } else {
            getFragmentManager().popBackStack();
        }
    }

    public void buttonOnClick(View v) {
        switch (v.getId()) {
            case R.id.Exit:
                finish();

                break;
            case R.id.Simple:

                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container, new Fragment_simple()).addToBackStack(null);
                fragmentTransaction.commit();
                break;


            case R.id.Advanced:

                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container, new Fragment_advanced()).addToBackStack(null);
                fragmentTransaction.commit();
                break;


            case R.id.About:

                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container, new Fragment_about()).addToBackStack(null);
                fragmentTransaction.commit();
                break;


        }

    }
}
